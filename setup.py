from setuptools import setup
setup(
    name='hype',
    version='1.0',
    py_modules=['hype'],
    install_requires = ['Markdown>=2.5'],
)