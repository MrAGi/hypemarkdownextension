import markdown
from markdown.preprocessors import Preprocessor
from markdown.extensions import Extension
import re

class HypePreprocessor(Preprocessor):
    def run(self,lines):
        new_lines = []
        hype = "^!\[hype\]\[.+\]\(.+\)"
        for line in lines:
            if re.match(hype, line):
                hype_text = re.split("\"|\(|\[|\]",line)
                hype_id = hype_text[3]
                hype_url = hype_text[5][:-1]
                hype_title = hype_text[6]
                html = """<figure>
    <div id="{0}_hype_container" style="position:relative;overflow:hidden;width:400px;height:300px;">
        <script type="text/javascript" src="{1}"></script>
    </div>
    <figcaption>{2}</figcaption>
</figure>""".format(hype_id,hype_url,hype_title)
                new_lines.append(html)
            else:
                new_lines.append(line)
        return new_lines

class HypeExtension(Extension):
    def extendMarkdown(self,md,md_globals):
        md.preprocessors.add('hype',HypePreprocessor(md),"<reference")

def makeExtension(**kwargs):
    return HypeExtension(**kwargs)
